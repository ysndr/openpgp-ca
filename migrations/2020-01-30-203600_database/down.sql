DROP TABLE if exists cas;
DROP TABLE if exists cacerts;
DROP TABLE if exists usercerts;
DROP TABLE if exists emails;
DROP TABLE if exists certs_emails;
DROP TABLE if exists revocations;
DROP TABLE if exists bridges;
--DROP TABLE if exists prefs;
